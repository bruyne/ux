 01 UX基础入门

UX：USer Experience 用户体验：用户与产品互动或体验产品时的感受。 

​    Usable: 容易使用

​	Equitable：受众更多，为所有人设计

​	Enjoyable：用户与产品之间建立联系

​	Useful：解决问题

Empathy：理解他人感受



UX designers：

 Interaction designers 交互设计师：专注于设计产品的体验和功能

Visual designers 视觉设计师：专注于产品或技术的外观

Motion designers 动作设计师： 考虑用户浏览产品时的感受



UX designers 的关键任务：每个产品的设计都应该让用户有特定的感觉



Responsibilities of entry-level UX designers

researching :通过研究了解用户。。。

wireframing：线框是产品或屏幕的轮廓或草图（基本结构框架）

prototyping：原型，产品的早期模型

creating information architecture：信息架构：网站的框架，如何组织，分类和结构。

communicating effectively：有效沟通





T shape designer



02  工具 术语 平台

end user:

user experience: 用户体验是指用户与产品互动或体验时的感受

UX designer :把终端用户放到最高优先级



user-centered design

undestand:理解 了解用户，或类似产品（）

specify：指定 最终用户的需求

design：设计 为最终用户的问题设计解决方案

evaluate：评估  根据最终用户的问题，评估设计



iteration 迭代 （key）

通过构建以前的版本来重做一些事情，并调整



framework 框架 项目的大纲

五要素框架 ： 把想法转化为可工作产品的步骤框架

 	（底层）strategy 策略   定义用户需求和业务目标

​					scope  范围   产品中包含的功能和内容

​					structure  结构  组织设计，用户如何与设计交互

​					skeleton  骨架  设计是如何工作的

​	（顶层）surface  表面  产品在用户眼中的样子



设计思维 创造解决方案的方式

​	empathize 移情 像用户一样思考，感受

​	define  创造问题陈述来定义问题  ，用户需要解决的问题

​	ideate   构思解决方案

​	prototype  原型 显示重要功能的产品的缩小版本

​	test  测试 用用户测试原型，根据用户反馈改进













三种将用户放在首位的设计方法

通用设计 ：  更多人参与到设计，为每个人设计（一刀切）

​		

包容设计： 满足不同需求的解决方案  ，做出设计选择



公平性设计 ： 为被低估或忽视的群体设计，





四项挑战

coast  成本 

connectivity  连通性 （离线）

digital literacy    数组素养 （指导不会使用）

literacy 读写能力 







工具 

使用工具 Figma adobeXD





平台 

用户使用不同的平台



用户的行为因设备而产生差异，用户体验师要帮助这种转变尽可能无缝地发生。



辅助技术 AT

色彩修改

语音控制 ，开关

屏幕阅读器

替代文本（替代图像等，用文字）









03 设计冲刺的5个阶段

 sprint

design sprint 是一个有时间限制的过程，通常分为五个阶段，每天八个小时。目标是通过设计、与用户一起创建原型并测试想法。



五个阶段

understand 理解:设计挑战的范围

将冲刺设定在正确的轨道上，并帮助团队清楚地了解设计挑战

ideate 构思：设想可能的解决方案

想出解决方案

decide 决定：决定可行的解决方案

决定构建哪些解决方案

prototype 原型：创建一个可行的原型

一些能够让用户进行测试的足够现实的东西

test 测试：实际用户测试原型

用户进行原型测试。



design sprint benefits

save time

create a path to bring a product to market

prioritize(优先考虑) the user 

test product before launch(上市)



Benefits of design sprints 设计冲刺的好处

It's all about the user 用户才是最重要的

Value every person in the room 重视房间里的每一个人

The best ideas rise to the top 最好的想法会脱颖而出

Time to focus 时间专注

Lowers risks  降低风险

Versatile scheduling  万能的（多用途的安排）









design sprints：

user research

call in the experts 

find the right space

gather supplies 收集补给

establish sprint rules

plan introductions

post-sprint planning







sprint brief 冲刺简报：

sprint challenge ：

KEY DELIVERABLES: 关键的可交付成果

logistics： 后勤问题

approvers： 批准者

resource：资源

project overview ：项目概述

sprint schedule ：项目计划



sprint retrospective :冲刺回顾  对团队设计冲刺阶段的合作批评

two key question : what went well 

​								 what can be improved













04  同理心

Empathy:The ability to understand someone else's feelings or thoughts in a situation    同理心: 在某种情况下理解别人的感受或想法的能力

Empathy map:An easily understood chart that explains everything designers have learned about a type of user  同理心地图： 一个易于理解的图表，它解释了设计师了解到的关于用户类型的一切

![QQ截图20220523153110](UX设计笔记.assets/QQ截图20220523153110.png)





Types of Pain Points

Financial

Product

Process

Support



user group

Benefits of personas：

Build empathy.

Tell stories

Stress-test designs



Advantages of User Stories

Prioritize design goals  优先考虑设计目标

Unite the team 团结团队

Inspire empathetic design decisions 激发共情的设计决策

Personalize pitches to stakeholders  为利益相关者提供个性化的建议

user story = hero + goal + conflict 





Edge case :What happens when things go wrong that are beyond the user's control

Spotting & resolving edgeecases

Create personas and user stories

Thoroughly review the project before launch

Use wireframes 线框图



User journey :The series of experiences a user has as they interact(相互作用) with your product



Benefits of user journey mapping：

Helps UX designers create obstacle-free paths for users    帮助用户体验设计师为用户创建无障碍路径

Reduces impact of designer bias 减少设计师偏见的影响

Highlights new pain points  突出新的痛点

Identify improvement opportunities   识别改进机会



绘制用户旅程的方法：

Step 1 ：Add each action in the journey until the user reaches their goal  

Step 2 ：Add descriptions for each action 

What tasks does the user have to do?

Step 3 ：Add how the user feels at each point Guesstimates are okay!

Step 4 : Add opportunities for improvement 

This is where new ideas may come from!



Curb-cut effect:

A phenomenon that describes how products and policies designed for people with disabilities often end up helping everyone

一种现象，描述了为残疾人设计的产品和政策如何最终帮助每个人



Problem statement best practices  问题陈述最佳实践

A clear description of the user's needs that should be addressed   应明确描述用户的需求



Problem statement best practices: 问题陈述最佳实践
Human-centered.
Broad enough for creative freedom. 足够宽广，可以自由创作
Narrow enough to be solved by a design



(name) is a (characteristics) who needs () because () 

if (action) then (outcome)



Problem statement best practices
Establish goals
Understand constraints   理解约束
Define deliverables   定义可交付成果
Create benchmarks for success   为成功建立基准





04-01

Mental models
Internal(内部) maps that allow humans to predict how something will work



Feedback loops 反馈循环
The outcome a user gets at the end of a process



Von Restorff effect/ Isolation effect               

Von Restorff效应/隔离效应

When multiple, similar objects are present, the one that differs from the rest is most likely to be remembered

当多个相似的物体出现时，与其他物体不同的那个最有可能被记住



Serial position effect

When given a list of items, people are more likely to remember the first few and the last few, while the items in the middle tend to blur

当给出一个项目列表时，人们更有可能记住前几项和后几项，而中间的项目往往模糊不清







05 设计构思过程



Ideation in the real world：
Brainstorm out loud 头脑风暴
Document all ideas Focus on quantity 记录所有想法，关注数量
Do not allow evaluation  不允许评估
Gather a diverse team  组建多元化的团队
Question the obvious 
Evaluate the ideas  评估的思想



评估：

Feasible: Technically possible to build

可行性:技术上可以建造

Desirable: Best at solving the user problem

可取的:最擅长解决用户问题

Viable: Financially beneficial for the business

可行性:在经济上对企业有利



Why should we come up with a lot of ideas?

List of ideas is narrowed based on constraints

基于各种限制条件，我们缩小了想法的范围

Need to focus on equity

需要关注公平

Let users test the ideas

让用户测试这些想法





竞争性审计

Identifying your key competitors

识别主要竞争对手

Reviewing the products that your competitors offer

评估竞争对手提供的产品

Understanding how your competitors position themselves in the market

了解你的竞争对手在市场上的定位

Examining what your competition does well and what they could do better

研究你的竞争对手做得好的方面和他们可以做得更好的方面

Considering how your competitors talk about themselves

考虑你的竞争对手是如何谈论他们自己的





直接竞争对手，间接竞争对手



Benefits to competitive audits

Inform your design process

Solve usability problems

Reveal gaps in the market

Provide reliable evidence

Save time, money, and energy





Limitations of competitive audits：

Stifle creativity

Depend on how well you interpret the findings

Not all designs work in all use cases

Need to be done regularly



Competitive audit steps：竞争的审计步骤

Outline the goals

Create a spreadsheet with a list of your competitors 

创建一个电子表格，列出你的竞争对手

Call out the specific features you want to compare

找出您想要比较的特定特性

Research each company

研究每个公司

Analyze findings

分析调查结果

Summarize findings in a report

在报告中总结发现





Use the learnings from our competitive audit to come up with even more ideas：

Take your team through the matrix  列出优缺点

Identify low hanging fruit 确定首要目标

Brainstorm gaps or opportunities 进行头脑风暴对比差距或机会

Sort ideas 分类整合想法





Ways to create "how might we" phrases

Amp up the good 

加强好的方面

Explore the opposite

探索相反

Change a status quo

改变现状

Break the point-of-view into pieces

分解成小的观点







画草图

手绘，分成八块



为谁设计，需求是什么



 



06 用户研究

Foundational（基础） research Answers the questions:
What should we build?

What are the user problems?

How can we solve them?



 Design research Answers the question :How should we build it?



Post-launch（发布后） research Answers the question:Did we succeed?





UX researcher qualities
Empathy: Able to understand someone else's feelings or thoughts in a situation
Pragmatism 实用主义: Focused on reaching goals
Collaboration 协作能力: Can work with a range of people, personalities, and work styles





一手研究

二手研究

定量研究 侧重于通过计数或测量收集数据 what

定向研究：侧重观察，基于访谈 why

问卷：定性与定量的混合

kpi



Secondary research Benefits：

Saves time and money

Immediately accessible 

Backs up primary research



Secondary research Drawbacks：

No first-hand user interaction

No specific user feedback





Interviews：

Benefits：

Understand what users think and why

Ask follow up questions

Drawbacks：

Take time and money

Small sample size



问卷：

Benefits

Larger sample size

Fast

Inexpensive

Drawbacks

No in-depth feedback



可用性研究：

Benefits

Firsthand user interaction 

Challenge our assumptions

In-depth feedback 

Drawbacks

Only measure how easy a product is to use





六种偏见（偏差？）

Confirmation bias 确认偏差  先入为主的信念 

解决方法：

Ask open-ended questions
Actively listen
Include a large sample of users



False consensus bias 错误共识偏差  

解决方法：

Identify and articulate your own assumptions 

Survey large groups of people



Primacy bias 首要偏差

同下



Recency bias 近因性（近期？）

解决：笔记或录音



Implicit bias 隐形

解决：

反思行为

别人指出偏见



Sunk cost fallacy  沉没成本谬论 已经花费的时间，金钱

解决：

将项目分成小块

列出指定的点，决定停止or前进







08 从故事板到绘制线框



enpathy maps     why

personas

users    who

stories  what

user journey    why



目标陈述为设计提供了理想的解决方案，包含who what why,描述产品+好处

(who) is a (who) who needs (what) because (why)





storyboard 故事板

是工具，将用户面临的潜在解决方案可视化



Four elements of a storyboard：

Character ： 故事中的用户

Scene：  想象用户的使用环境

Plot： 设计的好处或解决方案

Narrative :描述用户的需求及如何解决问题



两种故事板：

Big picture storyboard： Focuses on the user experience  ，理解用户使用产品时的感受 how+ why



Close-up storyboard ：Focuses on the product  why  



Fidelity（保真度） ：How closely a design matches the look-and-feel of the final product



Low fidelity：

Lower amount of complexity  低复杂度

Less refined or polished 

Called "lo-fi" for short  低保真

用于快速获得想法，为探索留出空间



High fidelity：

Closely matches the look and feel of the final product

More refined or polished

Called "hi-fi" for short

在测试的地方使用



Wireframe 线框 ：A basic outline of a digital experience, like an app or website



Purposes of wireframes：

Establish the basic structure of a page

Highlight the intended function of the product

Save time and resources



文本用横线

图像图标用圆？

行动用矩形和圆

![QQ截图20220523185404](UX设计笔记.assets/QQ截图20220523185404.png)



Benefits of wireframes：

Inform the elements to include in your design

Catch problems early

Get stakeholders to focus on structure

Save time and effort

Iterate quickly







































