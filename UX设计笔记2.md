09 从图纸到数字线框

Information architecture (IA) 信息架构：

Organizes content to help users understand where they are in a product and where the information they want is

组织内容，以帮助用户了解他们在产品中的位置以及他们想要的信息在哪里



![QQ截图20220524101307](UX设计笔记2.assets/QQ截图20220524101307.png)



Importance of information architecture 信息架构的重要性:

Organizes and defines the overall structure for the app or site  组织和定义应用程序或网站的整体结构

Provides a high-level view of a product 提供产品的高级视图

Helps stakeholders review your designs 帮助涉众审查您的设计

Helps engineers understand how to organize the data  帮助工程师理解如何组织数据

Allows your ideas to grow and iterate with the design   允许你的想法随着设计不断发展和迭代





创建线框图的目的是建立一个页面的基本结构，并突出各元素的指导作用。



纸质线框图的好处：

快速

不贵

探索更多想法

集中注意在线框图





Moving from paper to digital wireframes

我的纸线框完成了吗？

纸线框收到反馈了吗？

我准备好考虑基本的视觉线索了吗





Transitioning to digital wireframes：

Use actual content for important pieces of text  重要的文本部分使用实际的内容，不用占位符

Hold back on adding expressive content  不添加表达性内容，关注功能





格式塔：

相似度，  相似元素有相似功能

接近度    近距离元素更相关

共同区域  同一区域元素组合在一起





10低保真模型

消除偏见



欺骗性模式：

roach motel    让用户陷入不想要的情况，很难脱身

forced continuity  强制连续

sneak into basket （捆绑销售）？

hiddden costs 隐性消费

confirmshaming  用户不作会感到羞愧

稀缺

























